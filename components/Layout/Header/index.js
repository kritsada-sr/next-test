import React, { Component } from 'react'
import Link from 'next/link'
import Styles from './index.scss'

class Header extends Component {
  render() {
    return (
      <div className={`topbar ${this.props.scrollTop > 30 && 'scroll-down'}`}>
        <div className="logo">
          <Link prefetch href="/">
            <a>
              <img src='static/logo.svg' />
            </a>
          </Link>
        </div>
        <style dangerouslySetInnerHTML={{ __html: Styles }} />
      </div>
    )
  }
}

export default Header
