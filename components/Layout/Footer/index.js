import React, { Component } from 'react'
import Link from 'next/link'
import Styles from './index.scss'

class Footer extends Component {
  render() {
    return (
      <div className={`footer ${this.props.loading && 'loading'}`} >
        <div className="menu">
          <ul>
            <li>
              <Link prefetch href="/terms">
                <a>Terms and Conditions</a>
              </Link>
            </li> |
            <li>
              <Link prefetch href="/privacy">
                <a>Privacy Policy</a>
              </Link>
            </li> |
            <li>
              <Link prefetch href="/retention">
                <a>Data Retention Policy</a>
              </Link>
            </li>
          </ul>
        </div>
        <style dangerouslySetInnerHTML={{ __html: Styles }} />
      </div>
    )
  }
}

export default Footer
