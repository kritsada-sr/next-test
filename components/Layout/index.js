import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Head from 'next/head'
import {
  Container,
} from 'reactstrap'
import Header from './Header'
import Footer from './Footer'
import Styles from '../../css/index.scss'
// import * as windowActions from '../../actions/windowActions'

class Layout extends Component {
  state = {
    scrollTop: 0
  }

  componentDidMount() {
    document.addEventListener('scroll', e => this.handleScroll(e))
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', e => this.handleScroll(e))
  }

  handleScroll(e) {
    this.setState({ scrollTop: e.srcElement.documentElement.scrollTop })
  }

  render() {
    return (
      <div id='app'>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <style dangerouslySetInnerHTML={{ __html: Styles }} />
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
          <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
          <script src="https://cdn.polyfill.io/v2/polyfill.min.js" />
        </Head>
        <Header scrollTop={this.state.scrollTop} />
        <Container>
          {this.props.children}
        </Container>
        <Footer loading={this.props.isLoading} />
      </div>
    )
  }
}

// const mapStateToProps = state => ({
//   count: state.count
// })

// const mapDispatchToProps = dispatch => ({
//   windowAction: bindActionCreators(windowActions, dispatch)
// })

// export default connect(mapStateToProps, mapDispatchToProps)(Layout)
export default Layout
