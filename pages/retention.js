import React, { Component } from 'react'
import fetch from 'isomorphic-fetch'
import { Layout } from '../components'

class Retention extends Component {
  static async getInitialProps() {
    const res = await fetch('http://api.dev.dotography.net/api/v1/agreement/data-retention-policy')
    const json = await res.json()
    return { data: json.data.content }
  }

  render() {
    return (
      <Layout isLoading={!this.props.data}>
        <div className="content">
          <div className="container retension-container" dangerouslySetInnerHTML={{ __html: this.props.data }}></div>
          {
            !this.props.data &&
            <img className="loading-icon" src='static/loading.gif' />
          }
        </div>
      </Layout>
    )
  }
}

export default Retention
