import React, { Component } from 'react'
// import withRedux from 'next-redux-wrapper'
import { Layout } from '../components'
import reducers from '../reducers'

class Home extends Component {
  render() {
    return (
      <Layout isLoading={false}>
        <div className="content">
          <div className="container about-container">
            <div className="row content-row content-app">
              <div className="col-sm-6 col-xs-12 text-content">
                <div className="content-title">
                  <p>LQID App</p>
                </div>
                <hr />
                <div className="content-sub-title">
                  <p>LQID makes shopping and selling easy and fun.</p>
                </div>
                <div className="content-description">
                  <p>Download LQID app to check-out the latest deals or set-up your own shop.</p>
                </div>
                <div className="content-application">
                  <div className="row text-center">
                    <div className="ios">
                      <img src="static/app-store.png" style={{ opacity: 0.5 }} />
                    </div>
                    <div className="android">
                      <img src="static/google-play-store.png" style={{ opacity: 0.5 }} />
                    </div>
                  </div>
                  <div className="coming-soon text-center">
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-xs-12 image-content">
                <img src="static/app.png" />
              </div>
            </div>
            <div className="row content-row content-chat">
              <div className="col-sm-6 col-sm-push-6 col-xs-12 text-content">
                <div className="content-title">
                  <p>LQID Chat</p>
                </div>
                <hr />
                <div className="content-sub-title">
                  <p>Buyers, chat with shops to check size, colours or ask any other questions.</p>
                  <p>Sellers, talk to your customers to arrange delivery.</p>
                </div>
                <div className="content-description">
                  <p>LQID Chat works exclusively with LQID App</p>
                </div>
                <div className="content-application">
                  <div className="row text-center">
                    <div className="ios">
                      <img src="static/app-store.png" style={{ opacity: 0.5 }} />
                    </div>
                    <div className="android">
                      <img src="static/google-play-store.png" style={{ opacity: 0.5 }} />
                    </div>
                  </div>
                  <div className="coming-soon text-center">
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-sm-pull-6 col-xs-12 image-content">
                <img src="static/chat.png" />
              </div>
            </div>
            <div className="row content-row content-contact">
              <div className="col-sm-6 col-xs-12 text-content">
                <div className="content-title">
                  <p>Contact</p>
                </div>
                <hr />
                <div className="content-description">
                  <p>Contact the LQID support team using LQID Chat. Or you can email us
                    <a href="mailto:support@lqid360.com">here</a>.
                  </p>
                </div>
              </div>
              <div className="col-sm-6 col-xs-12 image-content">
                <img src="static/contact.png" />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

// export default withRedux(reducers, null, null)(Home)
export default Home
