import React, { Component } from 'react'
import fetch from 'isomorphic-fetch'
import { Layout } from '../components'

class Privacy extends Component {
  static async getInitialProps() {
    const res = await fetch('http://api.dev.dotography.net/api/v1/agreement/privacy-policy')
    const json = await res.json()
    return { data: json.data.content }
  }

  render() {
    return (
      <Layout isLoading={!this.props.data}>
        <div className="content">
          <div className="container privacy-container" dangerouslySetInnerHTML={{ __html: this.props.data }}></div>
          {
            !this.props.data &&
            <img className="loading-icon" src='static/loading.gif' />
          }
        </div>
      </Layout>
    )
  }
}

export default Privacy
