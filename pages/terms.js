import React, { Component } from 'react'
// import withRedux from 'next-redux-wrapper';
import fetch from 'isomorphic-fetch'
import { Layout } from '../components'

class Terms extends Component {
  static async getInitialProps() {
    const res = await fetch('http://api.dev.dotography.net/api/v1/agreement/terms-and-conds')
    const json = await res.json()

    return { data: json.data.content }
  }

  render() {
    return (
      <Layout isLoading={!this.props.data}>
        <div className="content">
          <div className="container terms-container" dangerouslySetInnerHTML={{ __html: this.props.data }}></div>
          {
            !this.props.data &&
            <img className="loading-icon" src='static/loading.gif' />
          }
        </div>
      </Layout>
    )
  }
}

export default Terms
