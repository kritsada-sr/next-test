'user strict'

import 'isomorphic-fetch'

const callAPIMiddleware = () => {
  return next => (action) => {
    return next(action)
  }
}

export default callAPIMiddleware
